package com.example.restclient;

public class RestURIConstants {
	
	public static final String SERVER_URL = "https://jsonplaceholder.typicode.com";
	
	public static final String LIST_POST = "/posts";
	
	public static final String CREATE_POST = "/posts";
	
	public static final String GET_POST = "/posts/{id}";
	
	public static final String UPDATE_POST = "/posts/{id}";
	
	public static final String DELETE_POST = "/posts/{id}";
	
}
