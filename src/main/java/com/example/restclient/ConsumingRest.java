package com.example.restclient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import com.example.model.Posts;

public class ConsumingRest {

	static RestTemplate restTemplate = new RestTemplate();
	
	public static List<Posts> listPost() {
		String uri = RestURIConstants.SERVER_URL + RestURIConstants.LIST_POST;
		return restTemplate.exchange(uri, HttpMethod.GET, null, new ParameterizedTypeReference<List<Posts>>() {} ).getBody();
	
	}

	public static Posts getPost(String id) {
		String uri = RestURIConstants.SERVER_URL + RestURIConstants.GET_POST;
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", id);
		Posts post = restTemplate.getForObject(uri, Posts.class, params);
		return post;
	}
	
	public static Posts createPost(Posts post) {
		String uri = RestURIConstants.SERVER_URL + RestURIConstants.CREATE_POST;
		return restTemplate.postForObject(uri, post, Posts.class);
	}

	public static void updatePost(Posts post) {
		String uri = RestURIConstants.SERVER_URL + RestURIConstants.UPDATE_POST;
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", post.getId().toString());
		restTemplate.put(uri, post, params);
	}

	public static void deletePost(String id) {
		String uri = RestURIConstants.SERVER_URL + RestURIConstants.DELETE_POST;
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", id);
		restTemplate.delete(uri, params);

	}

}