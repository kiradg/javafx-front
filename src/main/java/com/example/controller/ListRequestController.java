package com.example.controller;

import java.util.List;
import java.util.function.Function;

import javax.annotation.PostConstruct;

import com.example.model.Posts;
import com.example.restclient.ConsumingRest;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;

import io.datafx.controller.ViewController;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TreeTableColumn;

@ViewController(value = "/fxml/ListRequest.fxml", title = "LIST REQUEST")
public class ListRequestController {

	@FXML
	private JFXTreeTableView<Posts> postsTable;
	@FXML
	private JFXTreeTableColumn<Posts, Integer> idColumn;
	@FXML
	private JFXTreeTableColumn<Posts, String> titleColumn;
	@FXML
	private JFXTreeTableColumn<Posts, String> bodyColumn;
	@FXML
	private JFXTextField searchField;

	private ObservableList<Posts> dataTable;


	@PostConstruct
	public void init() {
		List<Posts> listPosts = ConsumingRest.listPost();
		
		setupCellValueFactory(titleColumn, p -> p.getTitle() );
		setupCellValueFactory(bodyColumn, p -> p.getBody());
		setupCellValueFactory(idColumn, p -> p.getId());

		dataTable = FXCollections.observableArrayList(listPosts);
		postsTable.setRoot(new RecursiveTreeItem<>(dataTable, RecursiveTreeObject::getChildren));
		postsTable.setShowRoot(false);
		
		searchField.textProperty().addListener(setupSearchField(postsTable));
	}
	
	private ChangeListener<String> setupSearchField(final JFXTreeTableView<Posts> tableView) {
        return (o, oldVal, newVal) ->
            tableView.setPredicate(postProp -> {
                final Posts post = postProp.getValue();
                return Integer.toString(post.getId()).contains(newVal) || post.getBody().contains(newVal) || post.getTitle().contains(newVal);
//                		person.firstName.get().contains(newVal)
//                    || person.lastName.get().contains(newVal)
//                    || Integer.toString(person.age.get()).contains(newVal);
            });
    }

	@SuppressWarnings("unchecked")
	private <T> void setupCellValueFactory(JFXTreeTableColumn<Posts, T> column, Function<Posts, T> mapper) {
		column.setCellValueFactory((TreeTableColumn.CellDataFeatures<Posts, T> param) -> {
			if (column.validateValue(param)) {
				T m = mapper.apply(param.getValue().getValue());
				return (ObservableValue<T>) new ReadOnlyStringWrapper(m.toString());
			} else {
				return column.getComputedValue(param);
			}
		});
	}
}
