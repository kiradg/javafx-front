package com.example.controller;

import javax.annotation.PostConstruct;

import com.example.model.Posts;
import com.example.restclient.ConsumingRest;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import io.datafx.controller.ViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@ViewController(value = "/fxml/GetRequest.fxml", title = "GET REQUEST")
public class GetRequestController {

	@FXML
	private JFXTextField searchField;

	@FXML
	private JFXTextField titleText;

	@FXML
	private JFXTextArea bodyText;

	@FXML
	private JFXTextField idText;

	@FXML
	private JFXButton btnSearch;

	@FXML
	private JFXButton btnUpdate;
	
	@FXML
	private JFXButton btnDelete;
	
	@FXML
	private Label infoLabel;

	private Posts post = new Posts();

	@PostConstruct
	public void init() {

		btnSearch.addEventHandler(ActionEvent.ACTION, (e) -> {
			if (searchField.validate()) {
				try {
					post = ConsumingRest.getPost(searchField.getText());
					idText.setText(post.getId().toString());
					titleText.setText(post.getTitle());
					bodyText.setText(post.getBody());
					
					btnUpdate.setDisable(false);
					btnDelete.setDisable(false);
				} catch (Exception e1) {
					idText.clear();
					titleText.clear();
					bodyText.clear();
					
					btnUpdate.setDisable(true);
					btnDelete.setDisable(true);
					
					infoLabel.setText("Error al buscar");
				}
			}
		});

		btnUpdate.addEventHandler(ActionEvent.ACTION, (e) -> {

			if (idText.validate() && titleText.validate() && bodyText.validate()) {
				try {
					post.setTitle(titleText.getText());
					post.setBody(bodyText.getText());
					ConsumingRest.updatePost(post);
					infoLabel.setText("Actualizado");
				} catch (Exception e1) {
					infoLabel.setText("Error al actualizar");
				}
			}
		});
		
		btnDelete.addEventHandler(ActionEvent.ACTION, (e) -> {

			if (idText.validate() && titleText.validate() && bodyText.validate()) {
				try {
					ConsumingRest.deletePost(post.getId().toString());
					idText.clear();
					titleText.clear();
					bodyText.clear();
					
					btnUpdate.setDisable(true);
					btnDelete.setDisable(true);
					infoLabel.setText("Eliminado");
				} catch (Exception e1) {
					infoLabel.setText("Error al eliminar");
				}
			}
		});

	}
}
