package com.example.controller;

import javax.annotation.PostConstruct;

import com.example.model.Posts;
import com.example.restclient.ConsumingRest;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import io.datafx.controller.ViewController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

@ViewController(value = "/fxml/CreateRequest.fxml", title = "CREATE REQUEST")
public class CreateRequestController {

	@FXML
	private JFXTextField titleText;

	@FXML
	private JFXTextArea bodyText;

	@FXML
	private JFXButton btnCreate;

	@FXML
	private Label infoLabel;

	@PostConstruct
	public void init() {
		btnCreate.addEventHandler(ActionEvent.ACTION, (e) -> {
			if (titleText.validate() && bodyText.validate()) {
				try {
					Posts post = new Posts();
					post.setTitle(titleText.getText());
					post.setBody(bodyText.getText());
					post = ConsumingRest.createPost(post);
					infoLabel.setText("Creado");
					titleText.clear();
					bodyText.clear();
				} catch (Exception e1) {
					infoLabel.setText("Error al crear");
				}
			}
		});
	}
}