package com.example;

import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

import com.example.controller.MainController;
import com.jfoenix.assets.JFoenixResources;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.svg.SVGGlyph;
import com.jfoenix.svg.SVGGlyphLoader;

import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

public class MainApp extends Application {
	
	@FXMLViewFlowContext
	private ViewFlowContext flowContext;
	
	private static final double MIN_WIDTH = 824;
	private static final double MIN_HEIGHT = 600;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		new Thread(() -> {
			try {
				SVGGlyphLoader.loadGlyphsFont(getClass().getResourceAsStream("/fonts/icomoon.svg"), "icomoon.svg");
			} catch (IOException ioExc) {
				ioExc.printStackTrace();
			}
		}).start();
		
		Flow flow = new Flow(MainController.class);
        DefaultFlowContainer container = new DefaultFlowContainer();
        flowContext = new ViewFlowContext();
        flowContext.register("Stage", stage);
        flow.createHandler(flowContext).start(container);

        JFXDecorator decorator = new JFXDecorator(stage, container.getView());
        decorator.setCustomMaximize(true);
        decorator.setGraphic(new SVGGlyph(""));

        stage.setTitle("JFoenix Demo");

//        try {
//            Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
//            width = bounds.getWidth() / 2.5;
//            height = bounds.getHeight() / 1.35;
//        }catch (Exception e){ }
		
		Scene scene = new Scene(decorator, MIN_WIDTH, MIN_HEIGHT);
		final ObservableList<String> stylesheets = scene.getStylesheets();
		stylesheets.addAll(JFoenixResources.load("css/jfoenix-fonts.css").toExternalForm(),
                JFoenixResources.load("css/jfoenix-design.css").toExternalForm(),
                getClass().getResource("/css/jfoenix-main.css").toExternalForm());
		//stylesheets.addAll(getClass().getResource("/styles/jfoenix-main.css").toExternalForm());
		stage.setScene(scene);
		stage.setMinWidth(MIN_WIDTH);
		stage.setMinHeight(MIN_HEIGHT);
		stage.show();
		
	}

}
